#ifndef _SRT_SOURCE_H_
#define _SRT_SOURCE_H_

#ifndef WIN32_LEAN_AND_MEAN
# define WIN32_LEAN_AND_MEAN
#endif

#include "srt_wrapper.h"

#include <wininet.h>

#include <string>
#include <memory>
#include <atomic>
#include <thread>


class SRTSourceStream : public CSourceStream
{
public:
    SRTSourceStream(SRTSOCKET socket, HRESULT *phr, CSource *pSrc);
    ~SRTSourceStream();

    // --- CBaseOutputPin ---

    HRESULT Active();
    HRESULT Inactive();
    HRESULT DecideBufferSize(IMemAllocator *pAlloc, ALLOCATOR_PROPERTIES *pRequest);

    // --- CSourceStream ---

    HRESULT GetMediaType(CMediaType *pMediaType);
    HRESULT FillBuffer(IMediaSample *pSample);

private:
    SRTSOCKET m_socket;
    std::atomic<bool> m_waitCancelled;
};

class SRTSource : public CSource, public IAMFilterMiscFlags,
                  public IAMOpenProgress, public IFileSourceFilter
{
public:
    static CUnknown * WINAPI CreateInstance(IUnknown *pUnk, HRESULT *phr);
    ~SRTSource();

    DECLARE_IUNKNOWN

    // --- CUnknown ---

    STDMETHODIMP NonDelegatingQueryInterface(REFIID riid, __deref_out void ** ppv);

    // --- IAMFilterMiscFlags ---

    ULONG GetMiscFlags();

    // --- IAMOpenProgress ---

    HRESULT AbortOperation();
    HRESULT QueryProgress(LONGLONG *pllTotal, LONGLONG *pllCurrent);

    // --- IFileSourceFilter ---

    HRESULT GetCurFile(LPOLESTR *ppszFileName, AM_MEDIA_TYPE *pmt);
    HRESULT Load(LPCOLESTR pszFileName, const AM_MEDIA_TYPE *pmt);

    // --- SRTSource ---
    inline int GetPollId() const { return m_pollid; }

private:
    SRTSource(IUnknown *pUnk, HRESULT *phr);

    enum class Mode {
        CLIENT,
        SERVER,
        RENDEZVOUS
    };

    struct UrlInfo {
        std::wstring hostname;
        std::wstring adapter;
        INTERNET_PORT port;
        Mode mode;
    };

    static HRESULT ParseURL(const wchar_t *url, UrlInfo *info);
    static std::shared_ptr<ADDRINFOW> GetAddressInfo(const std::wstring & hostname,
        INTERNET_PORT port, int flags = 0);

    HRESULT OpenClient(const UrlInfo & info);
    HRESULT OpenServer(const UrlInfo & info);
    HRESULT OpenRendezvous(const UrlInfo & info);

private:
    int m_pollid;
    SRTSOCKET m_bindsock;
    SRTSourceStream *m_stream;
    std::wstring m_url;
    std::atomic<bool> m_loadCancelled;
    std::thread m_loadThread;
};

#endif 
