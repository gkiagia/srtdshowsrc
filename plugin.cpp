#include "SRTSource.h"

#include <initguid.h>
#include "guids.h"

// The name of the filter
static const WCHAR g_srtDshowSrcName[] = L"SRT DirectShow Source";

// The output media types of this filter
AMOVIESETUP_MEDIATYPE outMediaTypes[] = {
    { &MEDIATYPE_Stream, &MEDIASUBTYPE_MPEG2_TRANSPORT }
};

// The pin of the filter
AMOVIESETUP_PIN srtDshowSrc_OutputPin = {
    L"",            // Obsolete, not used.
    FALSE,          // Is this pin rendered?
    TRUE,           // Is it an output pin?
    FALSE,          // Can the filter create zero instances?
    FALSE,          // Does the filter create multiple instances?
    &GUID_NULL,     // Obsolete.
    NULL,           // Obsolete.
    1,              // Number of media types.
    outMediaTypes   // Pointer to media types.
};

// The filter specification
AMOVIESETUP_FILTER srtDshowSrc = {
    &CLSID_SRT_DirectShow_Source,   // CLSID
    g_srtDshowSrcName,              // Name
    MERIT_NORMAL,                   // Merit
    1,                              // Number of pins
    &srtDshowSrc_OutputPin          // Pin information
};

// The filter factories - used by AMovieDllRegisterServer2()
CFactoryTemplate g_Templates[] =
{
    {
        g_srtDshowSrcName,
        &CLSID_SRT_DirectShow_Source,
        &SRTSource::CreateInstance,
        NULL,                       // init function
        &srtDshowSrc                // filter specification
    }
};

// The number of factories exported - used by AMovieDllRegisterServer2()
int g_cTemplates = sizeof(g_Templates) / sizeof(g_Templates[0]);


///////////////////////////
// exported entry points //
///////////////////////////

STDAPI
DllRegisterServer()
{
    return AMovieDllRegisterServer2(TRUE);
}

STDAPI
DllUnregisterServer()
{
    return AMovieDllRegisterServer2(FALSE);
}

//
// DllEntryPoint
//
extern "C" BOOL WINAPI DllEntryPoint(HINSTANCE, ULONG, LPVOID);

BOOL APIENTRY 
DllMain(HANDLE hModule,
    DWORD  dwReason,
    LPVOID lpReserved)
{
    DbgInitialise(GetModuleHandle(NULL));
    DbgSetModuleLevel(LOG_ERROR, 5);
    DbgSetModuleLevel(LOG_TRACE, 4);
    return DllEntryPoint((HINSTANCE)(hModule), dwReason, lpReserved);
}
