# SRT DirectShow Source

This library is a DirectShow plugin that provides a source filter which receives data
from a network stream using the SRT protocol. The stream is then treated as MPEG2-TS
data and passed downstream to the next filter.

## Dependencies

### SRT

You can get libsrt from https://github.com/Haivision/srt.
Use the build instructions you will find there.
In the end, install this library in C:\srt

**Note**: due to a bug in the **srt/win/wintime.h** header, you will need to modify it:

Replace 
```
#include "haicrypt.h"
```
with
```
#define HAICRYPT_API
```

and also in **srt/platform_sys.h**, replace:
```
#include <win/wintime.h>
```
with
```
#include "win/wintime.h"
```

### Microsoft DirectShow Base Classes

The base classes library (strmbase.lib) is a static library that is provided
by Microsoft in the form of source code, as a sample in the Windows SDK.
Unfortunately, newer versions of the SDK do not include this library,
so the only way to get it is to install the Windows 7.1 SDK.

After installing the Windows 7.1 SDK, copy the contents of
C:/Program Files/Microsoft SDKs/Windows/v7.1/Samples/multimedia/directshow/baseclasses
somewhere under your user profile directory and build it with Visual Studio.

Although the Windows 10 SDK provides a binary form of this library, unfortunately
it doesn't link properly, so you will need to build it anyway.

## Building

1. Install Visual Studio 2017 (the Community edition is just fine)
 * Make sure to install support for C++, CMake integration and the Windows 10 SDK.
2. Open the project folder in Visual studio (File -> Open -> Folder)
3. Let Visual Studio automatically configure the project with CMake.
4. Change the location of the detected strmbase.lib to the one you have built from source:
 * Open CMakeCache.txt: CMake -> Cache -> View CMakeCache.txt -> CMakeLists.txt (project srtdshowsrc)
 * Locate all references to strmbase.lib and change their path 
 * Save and close the file
5. Compile (CMake -> Build All)

## Testing

1. Install GraphStudioNext (x64) and the LAV decoder
 * An easy way to do both at once is to install the K-Lite Codec Pack
2. Open GraphStudioNext
3. Select "Graph -> Insert Filter from DLL"
4. Locate and load the srtdshowsrc.dll
 * Before doing that, ensure that srt.dll can be found either in the system path
   or in the same directory as srtdshowsrc.dll
5. If in client mode, ensure the remote server is running
6. In the dialog that opens, select "URL" and type an srt URL, for instance: srt://192.168.1.1:6000
 * The stransmit options 'adapter' and 'mode' are fully supported here, so you can for example
   use things like srt://:6000?mode=listener
7. If in server mode, now is the time to start the remote client
8. Go to "Graph -> Insert Filter" and find the MPEG-2 Demultiplexer; click "Insert"
9. Make sure the output pin of SRT Source is connected to the input pin of the demux
 * If the SRT Source doesn't have a visible output pin, hit F5 to refresh
10. Right click on the demultiplexer box and select "Create PSI Pin"
11. Click the "Start" button on the toolbar to start the data flow
12. Now click the "Stop" button to stop the flow
13. Go to "Graph -> Insert Filter" and insert the "LAV Decoder" as well the "Video Renderer"
14. Connect the demultiplexer's video output pin (not the PSI one) to the LAV Decoder's input pin
15. Connect the decoder's output pin to the video renderer
16. Click the "Start" button again to see the video

### Debug messages

When compiled in debug mode, there are messages printed from within the code that
can be seen with "DebugView".
