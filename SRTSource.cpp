#include "SRTSource.h"
#include "guids.h"

static_assert(sizeof(OLECHAR) == sizeof(wchar_t), "UNICODE is required");

/* adjust for performance depending on the bitrate */
#define POOLED_BUFFERS 300
#define MTU 1500

/////////////////////
// SRTSourceStream //
/////////////////////

SRTSourceStream::SRTSourceStream(SRTSOCKET socket, HRESULT *phr, CSource *pSrc)
    : CSourceStream(NAME("SRTSourceStream"), phr, pSrc, L"Output"),
      m_socket(socket)
{
}

SRTSourceStream::~SRTSourceStream()
{
    SRTSource *ssource = static_cast<SRTSource*>(m_pFilter);
    srt_epoll_remove_usock(ssource->GetPollId(), m_socket);
    srt_close(m_socket);
}

HRESULT
SRTSourceStream::Active()
{
    DbgLog((LOG_TRACE, 3, L"SRTSourceStream::Active called"));
    m_waitCancelled = false;
    return CSourceStream::Active();
}

HRESULT
SRTSourceStream::Inactive()
{
    DbgLog((LOG_TRACE, 3, L"SRTSourceStream::Inactive called"));
    m_waitCancelled = true;
    return CSourceStream::Inactive();
}

HRESULT
SRTSourceStream::DecideBufferSize(
    IMemAllocator *pAlloc,
    ALLOCATOR_PROPERTIES *pRequest)
{
    DbgLog((LOG_TRACE, 3, L"DecideBufferSize called"));

    pRequest->cBuffers = POOLED_BUFFERS;
    pRequest->cbBuffer = MTU;

    ALLOCATOR_PROPERTIES actual;
    return pAlloc->SetProperties(pRequest, &actual);
}

HRESULT
SRTSourceStream::GetMediaType(CMediaType *pMediaType)
{
    DbgLog((LOG_TRACE, 3, L"GetMediaType called"));
    pMediaType->SetType(&MEDIATYPE_Stream);
    pMediaType->SetSubtype(&MEDIASUBTYPE_MPEG2_TRANSPORT);
    pMediaType->SetFormatType(&FORMAT_None);
    pMediaType->SetTemporalCompression(FALSE);
    pMediaType->SetSampleSize(0);
    pMediaType->SetVariableSize();
    return S_OK;
}

HRESULT
SRTSourceStream::FillBuffer(IMediaSample *pSample)
{
    SRTSource *ssource = static_cast<SRTSource*>(m_pFilter);
    try {
        SRT::epoll_wait(ssource->GetPollId(), SRT_EPOLL_IN, m_waitCancelled);
    } catch (HRESULT hr) {
        return hr;
    }

    BYTE *data;
    if (FAILED(pSample->GetPointer(&data))) {
        DbgLog((LOG_ERROR, 1, L"IMediaSample::GetPointer failed"));
        return E_FAIL;
    }

    int recv_len = srt_recvmsg(m_socket, (char*) data, pSample->GetSize());

    if (recv_len == SRT_ERROR) {
        DbgLog((LOG_ERROR, 1, L"srt_recvmsg failed - %S", srt_getlasterror_str()));
        return E_FAIL;
    } else if (recv_len == 0) {
        return S_FALSE;
    }

    DbgLog((LOG_TRACE, 5, L"received buffer, size=%d", recv_len));

    pSample->SetActualDataLength(recv_len);
    return S_OK;
}

///////////////
// SRTSource //
///////////////

CUnknown * WINAPI
SRTSource::CreateInstance(IUnknown *pUnk, HRESULT *phr)
{
    SRTSource *srtsrc = new SRTSource(pUnk, phr);
    return srtsrc;
}

SRTSource::SRTSource(IUnknown *pUnk, HRESULT *phr)
    : CSource(NAME("SRTSource"), pUnk, CLSID_SRT_DirectShow_Source),
      m_pollid(SRT_ERROR),
      m_bindsock(SRT_INVALID_SOCK),
      m_stream(nullptr)
{
    srt_startup();
    try {
        m_pollid = SRT::epoll_create();
        *phr = S_OK;
    } catch (HRESULT hr) {
        *phr = hr;
    }
}

SRTSource::~SRTSource()
{
    AbortOperation();
    delete m_stream;
    if (m_bindsock != SRT_INVALID_SOCK)
        srt_close(m_bindsock);
    if (m_pollid != SRT_ERROR)
        srt_epoll_release(m_pollid);
    srt_cleanup();
}

STDMETHODIMP
SRTSource::NonDelegatingQueryInterface(REFIID riid, __deref_out void ** ppv)
{
    if (riid == IID_IAMFilterMiscFlags) {
        return GetInterface((IAMFilterMiscFlags *)this, ppv);
    } else if (riid == IID_IAMOpenProgress) {
        return GetInterface((IAMOpenProgress *)this, ppv);
    } else if (riid == IID_IFileSourceFilter) {
        return GetInterface((IFileSourceFilter *)this, ppv);
    } else {
        return CSource::NonDelegatingQueryInterface(riid, ppv);
    }
}

ULONG
SRTSource::GetMiscFlags()
{
    return AM_FILTER_MISC_FLAGS_IS_SOURCE;
}

HRESULT
SRTSource::AbortOperation()
{
    m_loadCancelled = true;
    if (m_loadThread.joinable())
        m_loadThread.join();
    return S_OK;
}

HRESULT
SRTSource::QueryProgress(LONGLONG *pllTotal, LONGLONG *pllCurrent)
{
    CheckPointer(pllTotal, E_POINTER);
    CheckPointer(pllCurrent, E_POINTER);
    *pllTotal = 100;
    *pllCurrent = m_stream ? 100 : 50;
    return m_stream ? S_OK : VFW_S_ESTIMATED;
}

HRESULT
SRTSource::GetCurFile(LPOLESTR *ppszFileName, AM_MEDIA_TYPE *pmt)
{
    CheckPointer(ppszFileName, E_POINTER);

    DbgLog((LOG_TRACE, 2, L"GetCurFile - %s", m_url.c_str()));

    if (m_url.empty())
        return E_FAIL;

    size_t characters = m_url.size() + 1;
    *ppszFileName = (LPOLESTR) CoTaskMemAlloc(characters * sizeof(wchar_t));
    wcscpy_s(*ppszFileName, characters, m_url.c_str());

    return S_OK;
}

HRESULT
SRTSource::Load(LPCOLESTR pszFileName, const AM_MEDIA_TYPE *pmt)
{
    CheckPointer(pszFileName, E_POINTER);

    DbgLog((LOG_TRACE, 3, L"Load() called"));

    /* Load() can only be called once according to IFileSourceFilter docs.
     * After the URL is loaded, it is invalid to call it again */
    if (!m_url.empty())
        return E_ILLEGAL_METHOD_CALL;

    /* Parse the URL */

    HRESULT hr;
    UrlInfo info;
    if (FAILED(hr = ParseURL(pszFileName, &info)))
        return hr;

    DbgLog((LOG_TRACE, 1, L"Got hostname: '%s', port: %d, mode: %d, adapter: '%s'",
        info.hostname.c_str(), info.port, info.mode, info.adapter.c_str()));

    /* store URL for GetCurFile() */
    m_url = pszFileName;

    /* execute all network operations in a new thread */
    m_loadCancelled = false;
    m_loadThread = std::thread([this, info] {
        DbgLog((LOG_TRACE, 3, L"network connection thread started"));

        HRESULT hr;
        switch (info.mode) {
        case Mode::CLIENT:
            hr = OpenClient(info);
            break;
        case Mode::SERVER:
            hr = OpenServer(info);
            break;
        case Mode::RENDEZVOUS:
            hr = OpenRendezvous(info);
            break;
        }

        DbgLog((LOG_TRACE, 3, L"network connection thread exiting, hr=0x%X", hr));
    });

    DbgLog((LOG_TRACE, 3, L"Load succeeded"));
    return S_OK;
}

HRESULT 
SRTSource::ParseURL(const wchar_t *url, UrlInfo *info)
{
    wchar_t scheme[INTERNET_MAX_SCHEME_LENGTH];
    wchar_t hostname[INTERNET_MAX_HOST_NAME_LENGTH];
    wchar_t extra[INTERNET_MAX_PATH_LENGTH];
    URL_COMPONENTS urlComponents;

    ZeroMemory(&urlComponents, sizeof(urlComponents));
    urlComponents.dwStructSize = sizeof(urlComponents);
    urlComponents.lpszScheme = scheme;
    urlComponents.dwSchemeLength = INTERNET_MAX_SCHEME_LENGTH;
    urlComponents.lpszHostName = hostname;
    urlComponents.dwHostNameLength = INTERNET_MAX_HOST_NAME_LENGTH;
    urlComponents.lpszExtraInfo = extra;
    urlComponents.dwExtraInfoLength = INTERNET_MAX_PATH_LENGTH;

    DbgLog((LOG_TRACE, 2, L"Parsing...: %s", url));

    if (!InternetCrackUrl(url, wcslen(url), 0, &urlComponents)) {
        DbgLog((LOG_ERROR, 1, L"Failed to parse URL - error = 0x%X", GetLastError()));
        return E_INVALIDARG;
    }

    if (_wcsicmp(scheme, L"srt") != 0) {
        DbgLog((LOG_ERROR, 1, L"Invalid protocol - %s", scheme));
        return E_INVALIDARG;
    }

    if (urlComponents.nPort == 0) {
        DbgLog((LOG_ERROR, 1, L"Failed to extract port from URL"));
        return E_INVALIDARG;
    }

    info->port = urlComponents.nPort;

    /* Parse extra options from the URL */

    info->mode = Mode::CLIENT;

    if (urlComponents.dwExtraInfoLength > 1 && extra[0] == L'?') {
        wchar_t *token;
        wchar_t *context = NULL;

        token = wcstok_s(extra + 1, L"&", &context);
        while (token != NULL) {
            if (!_wcsnicmp(token, L"mode=", 5)) {
                if (!_wcsnicmp(token + 5, L"server", 6) || !_wcsnicmp(token + 5, L"listener", 8))
                    info->mode = Mode::SERVER;
                else if (!_wcsnicmp(token + 5, L"client", 6) || !_wcsnicmp(token + 5, L"caller", 6))
                    info->mode = Mode::CLIENT;
                else if (!_wcsnicmp(token + 5, L"rendezvous", 10))
                    info->mode = Mode::RENDEZVOUS;
                else
                    DbgLog((LOG_ERROR, 1, L"Invalid mode: %s", token + 5));
            }

            if (!_wcsnicmp(token, L"adapter=", 8)) {
                info->adapter = token + 8;
            }

            token = wcstok_s(NULL, L"&", &context);
        }
    }

    if (urlComponents.dwHostNameLength == 0) {
        if (info->mode == Mode::CLIENT) {
            DbgLog((LOG_TRACE, 2, L"Switching to server mode because no hostname was specified"));
            info->mode = Mode::SERVER;
        }

        if (info->mode == Mode::SERVER && !info->adapter.empty()) {
            info->hostname = info->adapter;
        }
    } else {
        info->hostname = hostname;
    }

    return S_OK;
}

std::shared_ptr<ADDRINFOW>
SRTSource::GetAddressInfo(const std::wstring & hostname, INTERNET_PORT port, int flags)
{
    ADDRINFOW *result;
    ADDRINFOW hints;

    ZeroMemory(&hints, sizeof(hints));
    hints.ai_flags = flags;
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_protocol = IPPROTO_UDP;

    wchar_t *sHostname = hostname.empty() ? L"0.0.0.0" : hostname.c_str();
    std::wstring sPort = std::to_wstring(port);

    int err = GetAddrInfoW(sHostname, sPort.c_str(), &hints, &result);
    if (err) {
        DbgLog((LOG_ERROR, 1, L"Failed to get address info for %s:%s - err = %d",
            sHostname, sPort.c_str(), err));
        return NULL;
    }

    return std::shared_ptr<ADDRINFOW>(result, [](ADDRINFOW *p) { FreeAddrInfoW(p); });
}

HRESULT 
SRTSource::OpenClient(const UrlInfo & info)
{
    std::shared_ptr<ADDRINFOW> addr = GetAddressInfo(info.hostname, info.port);
    if (!addr)
        return E_INVALIDARG;

    DbgLog((LOG_TRACE, 2, L"Creating client socket - family = %d, type = %d, proto = %d",
        addr->ai_family, addr->ai_socktype, addr->ai_protocol));

    SRTSOCKET socket = SRT_INVALID_SOCK;
    try {
        socket = SRT::open(addr->ai_family, addr->ai_socktype, addr->ai_protocol);
        SRT::setsockopt(socket, SRTO_RCVSYN, false);

        SRT::connect(socket, addr->ai_addr);

        SRT::epoll_add_usock(m_pollid, socket, SRT_EPOLL_IN);
        SRT::epoll_wait(m_pollid, SRT_EPOLL_IN, m_loadCancelled, std::chrono::seconds(5));
    }
    catch (HRESULT hr) {
        if (socket != SRT_INVALID_SOCK)
            srt_close(socket);
        return hr;
    }

    /* create pin and pass the socket to it */
    HRESULT hr = S_OK;
    SRTSourceStream *stream = new SRTSourceStream(socket, &hr, this);
    if (FAILED(hr)) {
        DbgLog((LOG_ERROR, 1, L"Failed to create SRTSourceStream - hresult = 0x%X", hr));
        delete stream;
        return hr;
    }

    m_stream = stream;
    return S_OK;
}

HRESULT 
SRTSource::OpenServer(const UrlInfo & info)
{
    std::shared_ptr<ADDRINFOW> addr = GetAddressInfo(info.hostname, info.port, AI_PASSIVE);
    if (!addr)
        return E_INVALIDARG;

    DbgLog((LOG_TRACE, 2, L"Creating server socket - family = %d, type = %d, proto = %d",
        addr->ai_family, addr->ai_socktype, addr->ai_protocol));

    SRTSOCKET bindsock = SRT_INVALID_SOCK;
    SRTSOCKET socket = SRT_INVALID_SOCK;
    try {
        bindsock = SRT::open(addr->ai_family, addr->ai_socktype, addr->ai_protocol);
        SRT::setsockopt(bindsock, SRTO_RCVSYN, false);

        SRT::bind(bindsock, addr->ai_addr);
        SRT::listen(bindsock);

        SRT::epoll_add_usock(m_pollid, bindsock, SRT_EPOLL_IN);
        SRT::epoll_wait(m_pollid, SRT_EPOLL_IN, m_loadCancelled);
        srt_epoll_remove_usock(m_pollid, bindsock);

        socket = SRT::accept(bindsock);
        SRT::setsockopt(socket, SRTO_RCVSYN, false);
        SRT::epoll_add_usock(m_pollid, socket, SRT_EPOLL_IN);
    }
    catch (HRESULT hr) {
        if (socket != SRT_INVALID_SOCK)
            srt_close(socket);
        if (bindsock != SRT_INVALID_SOCK)
            srt_close(bindsock);
        return hr;
    }

    DbgLog((LOG_TRACE, 3, L"Got client connection"));

    /* create pin and pass the socket to it */
    HRESULT hr = S_OK;
    SRTSourceStream *stream = new SRTSourceStream(socket, &hr, this);
    if (FAILED(hr)) {
        DbgLog((LOG_ERROR, 1, L"Failed to create SRTSourceStream - hresult = 0x%X", hr));
        delete stream;
        srt_close(bindsock);
        return hr;
    }

    m_bindsock = bindsock;
    m_stream = stream;
    return S_OK;
}

HRESULT 
SRTSource::OpenRendezvous(const UrlInfo & info)
{
    std::shared_ptr<ADDRINFOW> bindaddr = GetAddressInfo(info.adapter, info.port, AI_PASSIVE);
    if (!bindaddr)
        return E_INVALIDARG;
    std::shared_ptr<ADDRINFOW> addr = GetAddressInfo(info.hostname, info.port);
    if (!addr)
        return E_INVALIDARG;

    DbgLog((LOG_TRACE, 2, L"Creating rendezvous socket - family = %d, type = %d, proto = %d",
        bindaddr->ai_family, bindaddr->ai_socktype, bindaddr->ai_protocol));

    SRTSOCKET socket = SRT_INVALID_SOCK;
    try {
        socket = SRT::open(bindaddr->ai_family, bindaddr->ai_socktype, bindaddr->ai_protocol);
        SRT::setsockopt(socket, SRTO_RCVSYN, false);
        SRT::setsockopt(socket, SRTO_RENDEZVOUS, true);
        SRT::bind(socket, bindaddr->ai_addr);
        SRT::connect(socket, addr->ai_addr);
    }
    catch (HRESULT hr) {
        if (socket != SRT_INVALID_SOCK)
            srt_close(socket);
        return hr;
    }

    /* create pin and pass the socket to it */
    HRESULT hr = S_OK;
    SRTSourceStream *stream = new SRTSourceStream(socket, &hr, this);
    if (FAILED(hr)) {
        DbgLog((LOG_ERROR, 1, L"Failed to create SRTSourceStream - hresult = 0x%X", hr));
        delete stream;
        return hr;
    }

    m_stream = stream;
    return S_OK;
}
