#include "srt_wrapper.h"

namespace SRT {

    void _epoll_wait(
        int id,
        SRT_EPOLL_OPT d,
        const std::atomic<bool> & cancelled,
        const std::chrono::milliseconds & timeout)
    {
        int nready = 2;
        SRTSOCKET ready[2];

        using namespace std::chrono;
        bool use_timeout = (timeout != milliseconds(-1));
        auto start_time = use_timeout ? steady_clock::now() : steady_clock::time_point();

        while (true) {
            /* we don't really care about which sockets are ready,
               we only want to give a dummy buffer to epoll_wait in order
               to tell it which kind of events to watch for */
            int err = srt_epoll_wait(id,
                (d == SRT_EPOLL_IN) ? ready : nullptr,
                (d == SRT_EPOLL_IN) ? &nready : nullptr,
                (d == SRT_EPOLL_OUT) ? ready : nullptr,
                (d == SRT_EPOLL_OUT) ? &nready : nullptr,
                20, nullptr, nullptr, nullptr, nullptr);

            /* check if someone cancelled the operation externally */
            if (cancelled) {
                DbgLog((LOG_TRACE, 2, L"SRT::epoll_wait aborted"));
                throw E_ABORT;
            }

            /* check if we timed out */
            if (err == SRT_ERROR && srt_getlasterror(nullptr) == SRT_ETIMEOUT) {
                if (use_timeout) {
                    auto now = steady_clock::now();
                    auto dur = duration_cast<milliseconds>(now - start_time);
                    if (dur > timeout) {
                        DbgLog((LOG_TRACE, 2, L"SRT::epoll_wait timed out"));
                        throw VFW_E_TIMEOUT;
                    }
                }
            }
            /* or if there was another error */
            else if (err == SRT_ERROR) {
                DbgLog((LOG_ERROR, 1, L"srt_epoll_wait error - %S",
                    srt_getlasterror_str()));
                throw E_FAIL;
            }
            else
                break;
        }
    }

}