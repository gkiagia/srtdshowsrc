#ifndef _SRT_WRAPPER_H_
#define _SRT_WRAPPER_H_

#ifndef WIN32_LEAN_AND_MEAN
# define WIN32_LEAN_AND_MEAN
#endif
#include <srt/srt.h>
#include <streams.h>

#include <atomic>
#include <chrono>

/* C++-ized SRT API with default & dummy values filled in, plus error handling */
namespace SRT
{
    inline SRTSOCKET open(int af, int type, int protocol)
    {
        SRTSOCKET sock = srt_socket(af, type, protocol);
        if (sock == SRT_INVALID_SOCK) {
            DbgLog((LOG_ERROR, 1, L"Failed to create SRT socket - %S",
                srt_getlasterror_str()));
            throw E_FAIL;
        }
        return sock;
    }

    inline SRTSOCKET accept(SRTSOCKET u)
    {
        sockaddr saddr;
        int saddrlen = sizeof(sockaddr);

        SRTSOCKET socket = srt_accept(u, &saddr, &saddrlen);
        if (socket == SRT_INVALID_SOCK) {
            DbgLog((LOG_ERROR, 1, L"Failed to accept connection - %S",
                srt_getlasterror_str()));
            throw E_FAIL;
        }
        return socket;
    }

    inline void bind(SRTSOCKET u, const struct sockaddr *name)
    {
        int err = srt_bind(u, name, sizeof(*name));
        if (err == SRT_ERROR) {
            DbgLog((LOG_ERROR, 1, L"Failed to bind - %S",
                srt_getlasterror_str()));
            throw E_FAIL;
        }
    }

    inline void listen(SRTSOCKET u, int backlog = 1)
    {
        int err = srt_listen(u, backlog);
        if (err == SRT_ERROR) {
            DbgLog((LOG_ERROR, 1, L"Failed to listen - %S",
                srt_getlasterror_str()));
            throw E_FAIL;
        }
    }

    inline void connect(SRTSOCKET u, const struct sockaddr *name)
    {
        int err = srt_connect(u, name, sizeof(*name));
        if (err == SRT_ERROR) {
            DbgLog((LOG_ERROR, 1, L"Failed to connect - %S",
                srt_getlasterror_str()));
            throw E_FAIL;
        }
    }

    template<typename T>
    inline void setsockopt(SRTSOCKET u, SRT_SOCKOPT optname, const T & value)
    {
        int err = srt_setsockopt(u, 0, optname, &value, sizeof(T));
        if (err == SRT_ERROR) {
            DbgLog((LOG_ERROR, 1, L"Failed to set socket option - %S",
                srt_getlasterror_str()));
            throw E_FAIL;
        }
    }

    inline int epoll_create()
    {
        int id = srt_epoll_create();
        if (id == SRT_ERROR) {
            DbgLog((LOG_ERROR, 1, L"Failed to create poll id - %S",
                srt_getlasterror_str()));
            throw E_FAIL;
        }
        return id;
    }

    /* same call but without the pointer parameter */
    inline void epoll_add_usock(int id, SRTSOCKET sock, const SRT_EPOLL_OPT events)
    {
        srt_epoll_add_usock(id, sock, (int*) &events);
    }

    void _epoll_wait(int id, SRT_EPOLL_OPT d, const std::atomic<bool> & cancelled,
        const std::chrono::milliseconds & timeout);

    /* waits until ready, cancelled or timed out */
    template <typename T, typename R>
    inline void epoll_wait(
        int id,
        SRT_EPOLL_OPT d, 
        const std::atomic<bool> & cancelled,
        const std::chrono::duration<T, R> & timeout)
    {
        using namespace std::chrono;
        _epoll_wait(id, d, cancelled, duration_cast<milliseconds>(timeout));
    }

    /* waits until ready or cancelled; there is no timeout */
    inline void epoll_wait(
        int id,
        SRT_EPOLL_OPT d,
        const std::atomic<bool> & cancelled)
    {
        _epoll_wait(id, d, cancelled, std::chrono::milliseconds(-1));
    }
};

#endif
